import requests


import json
#url="https://totaltechnology.atlassian.net//rest/api/2/issue"

def postData(testCase):
    url = "https://lotterywest.atlassian.net/rest/api/3/issue"
    headers={
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    payload=json.dumps(
        {
        "fields": {
           "project":
           {
              "key": "CBAPTEST"
           },
           "summary": testCase,
           "description": {
              "type": "doc",
              "version": 1,
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "type": "text",
                      "text": ""
                    }
                  ]
                }
              ]
            },
           "issuetype": {
              "name": "Task"
           }
       }
    }
    )


    response=requests.post(url,headers=headers,data=payload,auth=("raj.tandukar@lotterywest.wa.gov.au","IlUxAoG3faEtReVeljt8DB7D"))
    data=response.json()

    return data
